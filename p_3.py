import numpy as np
import matplotlib.pyplot as plt

# print(np.__version__)
class_grade = np.loadtxt('score.csv', dtype=str, delimiter=',', skiprows=1)

# ,name,Math,Physics,Astronomy,Quantum Mechanics
total_score = []
dict_math = {}
math = []
physics = []
astronomy = []
QM = []
dict_physics = {}
dict_astronomy = {}
dict_QM= {}
dict_score = {}

# print(class_grade)
# 字典{名字：}
k = 1
# print(class_grade[64])
for i in range(len(class_grade)):
    # print(i,len(class_grade))
    t = 0
    l = 2
    while l < 6:
        t += int(class_grade[i][l])
        l += 1
    total_score.append(t)  
# print(total_score)
for i in range(len(class_grade)-1):
    dict_score[class_grade[i][1]] = total_score[i]
    dict_math[class_grade[i][1]] = class_grade[i][2]
    math.append(int(class_grade[i][2]))
    dict_physics[class_grade[i][1]] = class_grade[i][3]
    physics.append(int(class_grade[i][2]))
    dict_astronomy[class_grade[i][1]] = class_grade[i][4]
    astronomy.append(int(class_grade[i][2]))
    dict_QM[class_grade[i][1]] = class_grade[i][5]
    QM.append(int(class_grade[i][2]))
    
# 发现有两个Nikoo...

print(total_score)
print(math)
# 分布图
x = np.linspace(0,len(class_grade),len(class_grade))
# plt.xlabel('total score')
# plt.ylabel('number')
plt.subplot(3,1,1)
plt.xlabel('total score')
plt.ylabel('number')
plt.hist(total_score,color='r', bins=15)
plt.subplots_adjust(hspace=0.5)

plt.subplot(3,2,3)
plt.hist(math,color='b',bins=15)
plt.xlabel('math score')
plt.ylabel('number')
plt.subplots_adjust(wspace=0.5)

plt.subplot(3,2,4)
plt.hist(astronomy,bins=15)
plt.xlabel('QM score')
plt.ylabel('number')

plt.show()
